<?php

use JoliCode\Slack\Api\Model\ObjsUser;
use JoliCode\Slack\ClientFactory;
use JoliCode\Slack\Exception\SlackErrorResponse;

// require_once __DIR__.'/../../vendor/autoload.php';
require_once '../../../../autoload.php';
$slackOAuthToken ='xoxp-865028107878-862362783444-851363970403-0b3cf2a497672ef0a4f9c6b0f5d334fd';

$client = ClientFactory::create($slackOAuthToken);
/** @var ObjsUser[] $users */
$users = [];
$cursor = '';

try {
    do {
        // This method requires your token to have the scope "users:read"
        $response = $client->usersList([
            'limit' => 200,
            'cursor' => $cursor,
        ]);
        // echo '<pre>';print_r($response->getMembers());exit();
        $users = array_merge($users, $response->getMembers());
        echo '<pre>';print_r($users);exit();

        $cursor = $response->getResponseMetadata() ? $response->getResponseMetadata()->getNextCursor() : '';
    } while (!empty($cursor));

    echo sprintf('Here is the names of the members of your workspace: %s', implode(', ', array_map(function(ObjsUser $user) {
        return $user->getName();
    }, $users)));
} catch (SlackErrorResponse $e) {
    echo 'Fail to retrieve the members.', PHP_EOL, $e->getMessage();
}
