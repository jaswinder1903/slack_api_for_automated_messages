<?php

use JoliCode\Slack\Api\Model\ObjsUser;
use JoliCode\Slack\ClientFactory;
use JoliCode\Slack\Exception\SlackErrorResponse;
require_once '../../../../autoload.php';

$slackOAuthToken='xoxp-865028107878-862362783444-851363970403-0b3cf2a497672ef0a4f9c6b0f5d334fd';//get you own token by oAuth
$client = ClientFactory::create($slackOAuthToken);

/** @var ObjsUser[] $users */
$userId;
try {
        // This method requires your token to have the scope "users:read"
        $response = $client->usersLookupByEmail([
            'email'=>'narender.kumar@classicinformatics.com',
            'token'=>$slackOAuthToken
        ])->getUser();
        $userId=$response->getId(); 
        if(isset($userId) && $userId != null)
        {
            try {
                // This method requires your token to have the scope "chat:write"
                $result = $client->chatPostMessage([
                    'channel' => $userId,
                    'text' => 'This is an automated message for testing.',
                    'as_user'=>true
                ]);
            
                echo 'Message sent to '.$response->getName();
            } catch (SlackErrorResponse $e) {
                echo 'Fail to send the message.', PHP_EOL, $e->getMessage();
            }
        }
} 
catch (SlackErrorResponse $e) 
{
    echo 'Fail to retrieve the members.', PHP_EOL, $e->getMessage();
}



// return $members;
// echo '<pre>';print_r($memberss);exit();